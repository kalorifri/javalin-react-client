/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectUsername = () =>
  createSelector(
    selectHome,
    homeState => homeState.username,
  );

const makeSelectNewBoxValidationResultColor = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBoxValidationResult.color,
  );

const makeSelectNewBoxValidationResultCountry = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBoxValidationResult.country,
  );

const makeSelectNewBoxValidationResultName = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBoxValidationResult.name,
  );

const makeSelectNewBoxValidationResultWeight = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBoxValidationResult.weight,
  );

const makeSelectNewBox = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBox,
  );

const makeSelectNewBoxColorObj = () =>
  createSelector(
    selectHome,
    homeState => homeState.colorObj,
  );

const makeSelectNewBoxColor2 = () => {
  return createSelector(
    selectHome,
    homeState => homeState.newBox.color,
  );
};

const makeSelectNewBoxCountry = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBox.country,
  );

const makeSelectNewBoxName = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBox.name,
  );

const makeSelectNewBoxWeight = () =>
  createSelector(
    selectHome,
    homeState => homeState.newBox.weight,
  );

const makeSelectBoxes2 = () =>
  createSelector(
    selectHome,
    homeState => homeState.boxes2,
  );

export {
  selectHome,
  makeSelectUsername,
  makeSelectNewBox,
  makeSelectNewBoxColor2,
  makeSelectNewBoxColorObj,
  makeSelectNewBoxCountry,
  makeSelectNewBoxName,
  makeSelectNewBoxWeight,
  makeSelectNewBoxValidationResultColor,
  makeSelectNewBoxValidationResultCountry,
  makeSelectNewBoxValidationResultName,
  makeSelectNewBoxValidationResultWeight,
  makeSelectBoxes2
};
