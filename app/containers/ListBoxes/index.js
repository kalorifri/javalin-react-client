/*
 * ListBoxes
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import BoxesList from 'components/BoxesList';

import validation from 'components/BoxForm/validation';

import H2 from 'components/H2';
// import ReposList from 'components/ReposList';
import AtPrefix from './AtPrefix';
import CenteredSection from './CenteredSection';
import Form from './Form';
import Input from './Input';
import Section from './Section';
import messages from './messages';

import {
  changeUsername,
  changeNewBoxColor2,
  changeNewBoxColorObj,
  changeNewBoxCountry,
  changeNewBoxName,
  changeNewBoxWeight,
  getBoxes,
  saveNewBox,
} from './actions';

import {
  makeSelectUsername,
  makeSelectNewBox,
  makeSelectNewBoxValidationResultColor,
  makeSelectNewBoxValidationResultCountry,
  makeSelectNewBoxValidationResultName,
  makeSelectNewBoxValidationResultWeight,
  makeSelectNewBoxCountry,
  makeSelectNewBoxColor2,
  makeSelectNewBoxColorObj,
  makeSelectNewBoxName,
  makeSelectNewBoxWeight,
  makeSelectBoxes2,
} from './selectors';

import reducer from './reducer';
// import { changeNewBoxColor2 } from '../App/actions';
import saga from './saga';

import '../../css/app.css';

import { RgbColorPicker } from 'react-colorful';
// import "react-colorful/dist/index.css";

const key = 'home';

export function ListBoxes({
  username,
  newBoxColor2,
  newBoxColorObj,
  newBoxCountry,
  newBoxName,
  newBoxWeight,
  boxes2,
  newBoxNameValidationColor,
  newBoxNameValidationCountry,
  newBoxNameValidationName,
  newBoxNameValidationWeight,

  onChangeUsername,
  onChangeNewColorObj,
  onChangeNewBoxColor2,
  onChangeNewBoxCountry,
  onChangeNewBoxName,
  onChangeNewBoxWeight,
  onGetBoxes,
  onSaveNewBox,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    // When initial state username is not null, submit the form to load repos
    // if (username && username.trim().length > 0) onSubmitForm();

    onGetBoxes();
  }, []);

  const reposListProps = {};

  const countries = [
    { name: 'Austraila', id: 'Austraila', default: false },
    { name: 'Brazil', id: 'Brazil', default: false },
    { name: 'China', id: 'China', default: false },
    { name: 'Sweden', id: 'Sweden', default: true },
  ];
  const countriesOptions = countries.map(country => (
    <option
      key={`newBoxCountryOption${country.id}`}
      id={`newBoxCountryOption${country.id}`}
      value={country.id}
    >
      {country.name}
    </option>
  ));

  return (
    <article>
      <Helmet>
        <title>Boxes</title>
        <meta name="description" content="List of boxes" />
      </Helmet>
      <div>
        <div className="container">
          <div className="row">
            <div className="col col-12 p-2">
              <h2 className="list-title float-start">Boxes</h2>
              <Link to="./addbox" className="btn btn-primary float-end">
                + New box
              </Link>
            </div>
          </div>
        </div>
        <BoxesList boxes={boxes2} />
      </div>
    </article>
  );
}

ListBoxes.propTypes = {
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  username: makeSelectUsername(),
  newBoxColor2: makeSelectNewBoxColor2(),
  newBoxColorObj: makeSelectNewBoxColorObj(),
  newBoxCountry: makeSelectNewBoxCountry(),
  newBoxName: makeSelectNewBoxName(),
  newBoxNameValidationColor: makeSelectNewBoxValidationResultColor(),
  newBoxNameValidationCountry: makeSelectNewBoxValidationResultCountry(),
  newBoxNameValidationName: makeSelectNewBoxValidationResultName(),
  newBoxNameValidationWeight: makeSelectNewBoxValidationResultWeight(),
  newBoxWeight: makeSelectNewBoxWeight(),
  boxes2: makeSelectBoxes2(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onChangeNewColorObj: colorObj => {
      dispatch(changeNewBoxColorObj(colorObj));
    },
    onChangeNewBoxColor2: evt => dispatch(changeNewBoxColor2(evt.target.value)),
    onChangeNewBoxCountry: evt =>
      dispatch(changeNewBoxCountry(evt.target.value)),
    onChangeNewBoxName: evt => dispatch(changeNewBoxName(evt.target.value)),
    onChangeNewBoxWeight: evt => dispatch(changeNewBoxWeight(evt.target.value)),
    onGetBoxes: () => dispatch(getBoxes()),
    onSaveNewBox: () => dispatch(saveNewBox()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ListBoxes);
