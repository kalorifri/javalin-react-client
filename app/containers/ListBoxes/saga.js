/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';

import request from 'utils/request';
import {
  saveBoxes,
} from './actions';

import { GET_BOXES, SAVE_NEW_BOX } from './constants';

export function* fetchBoxes() {
  try {
    // Call our request helper (see 'utils/request')
    const requestURL = 'http://localhost:7000/boxes';
    const boxes = yield call(request, requestURL);

    yield put(saveBoxes(boxes));
  } catch (err) {
    // silent fail
    console.log(`fetchBoxes error${err}`);
  }
}


/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  // yield takeLatest(LOAD_REPOS, getRepos);
  yield takeLatest(GET_BOXES, fetchBoxes);
}
