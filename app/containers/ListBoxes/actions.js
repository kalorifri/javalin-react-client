/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  CHANGE_USERNAME,
  CHANGE_NEW_BOX_COLOR_2,
  CHANGE_NEW_BOX_COLOR_OBJ,
  CHANGE_NEW_BOX_COUNTRY,
  CHANGE_NEW_BOX_NAME,
  CHANGE_NEW_BOX_WEIGHT,
  CHANGE_NEW_BOX_VALIDATION_RESULT,
  GET_BOXES,
  SAVE_BOXES,
  SAVE_NEW_BOX
} from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {string} username The new text of the input field
 *
 * @return {object} An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

export function changeNewBoxValidationResult(validationResult) {
  return {
    type: CHANGE_NEW_BOX_VALIDATION_RESULT,
    validationResult,
  };
}

export function changeNewBoxColorObj(color) {
  return {
    type: CHANGE_NEW_BOX_COLOR_OBJ,
    color,
  };
}

export function changeNewBoxColor2(color) {
  return {
    type: CHANGE_NEW_BOX_COLOR_2,
    color,
  };
}

export function changeNewBoxCountry(country) {
  return {
    type: CHANGE_NEW_BOX_COUNTRY,
    country,
  };
}

export function changeNewBoxName(name) {
  return {
    type: CHANGE_NEW_BOX_NAME,
    name,
  };
}

export function changeNewBoxWeight(weight) {
  return {
    type: CHANGE_NEW_BOX_WEIGHT,
    weight,
  };
}

export function saveNewBox(box) {

  console.log('action saveNewBox');
  return {
    type: SAVE_NEW_BOX,
    box: box
  };
}

export function getBoxes() {

  console.log('action getBoxes');
  return {
    type: GET_BOXES
  };
}

export function saveBoxes(boxes) {
  return {
    type: SAVE_BOXES,
    boxes,
  };
}