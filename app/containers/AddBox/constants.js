/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';
export const CHANGE_NEW_BOX_COLOR_2 = 'boilerplate/Home/CHANGE_NEW_BOX_COLOR_2';
export const CHANGE_NEW_BOX_COLOR_OBJ =
  'boilerplate/Home/CHANGE_NEW_BOX_COLOR_OBJ';
export const CHANGE_NEW_BOX_COUNTRY = 'boilerplate/Home/CHANGE_NEW_BOX_COUNTRY';
export const CHANGE_NEW_BOX_NAME = 'boilerplate/Home/CHANGE_NEW_BOX_NAME';
export const CHANGE_NEW_BOX_WEIGHT = 'boilerplate/Home/CHANGE_NEW_BOX_WEIGHT';
export const CHANGE_NEW_BOX_VALIDATION_RESULT =
  'boilerplate/Home/CHANGE_NEW_BOX_VALIDATION_RESULT';
export const GET_BOXES = 'boilerplate/Home/GET_BOXES';
export const SAVE_BOXES = 'boilerplate/Home/SAVE_BOXES';
export const SAVE_NEW_BOX = 'boilerplate/Home/SAVE_NEW_BOX';
