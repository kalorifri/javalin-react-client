/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import validation from 'components/BoxForm/validation';

import request from 'utils/request';
import { makeSelectNewBox } from './selectors';
import {
  saveBoxes,
  changeNewBoxWeight,
  changeNewBoxValidationResult,
} from './actions';

import { GET_BOXES, SAVE_NEW_BOX } from './constants';

export function* saveNewBox() {
  try {
    const newBox = yield select(makeSelectNewBox());

    // Validate new box before save
    const validationResult = validation(newBox);
    yield put(changeNewBoxValidationResult(validationResult));

    // Invalid weight, default to 0
    if (validationResult.weight !== '') {
      yield put(changeNewBoxWeight(0));
    }

    if (validationResult.hasError) {
      throw `Validation error${JSON.stringify(validationResult)}`;
    }

    const requestURL = 'http://localhost:7000/create-box';
    const result = yield call(request, requestURL, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newBox),
    });

    yield put(push('/listboxes'));
  } catch (err) {
    // silent fail
    console.log(`saveNewBox error: ${err}`);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  // yield takeLatest(LOAD_REPOS, getRepos);
  yield takeLatest(SAVE_NEW_BOX, saveNewBox);
}
