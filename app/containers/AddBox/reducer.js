/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  CHANGE_USERNAME,
  CHANGE_NEW_BOX_COLOR_2,
  CHANGE_NEW_BOX_COLOR_OBJ,
  CHANGE_NEW_BOX_COUNTRY,
  CHANGE_NEW_BOX_NAME,
  CHANGE_NEW_BOX_WEIGHT,
  CHANGE_NEW_BOX_VALIDATION_RESULT,
  SAVE_BOXES,
} from './constants';

import { saveNewBox } from './saga';

// The initial state of the App
export const initialState = {
  username: '',
  newBoxColor: 'blue',
  newBox: {
    color: '0,0,0',
    name: 'John',
    country: 'Sweden',
    weight: 0,
  },
  newBoxValidationResult: {
    color: '',
    country: '',
    name: '',
    weight: '',
    hasError: false,
  },
  colorObj: {
    r: 0,
    g: 0,
    b: 0,
  },
  boxes2: [],
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CHANGE_USERNAME:
        // Delete prefixed '@' from the github username
        draft.username = action.username.replace(/@/gi, '');
        break;
      case CHANGE_NEW_BOX_COLOR_2:
        draft.newBox.color = action.color;
        break;
      case CHANGE_NEW_BOX_COLOR_OBJ:
        console.log(
          `CHANGE_NEW_BOX_COLOR_OBJ: ${JSON.stringify(action.color)}`,
        );
        draft.colorObj = action.color;
        draft.newBox.color = `${action.color.r},${action.color.g},${
          action.color.b
        }`;
        break;
      case CHANGE_NEW_BOX_COUNTRY:
        draft.newBox.country = action.country;
        break;
      case CHANGE_NEW_BOX_NAME:
        draft.newBox.name = action.name;
        console.log('CHANGE_NEW_BOX_NAME');
        break;
      case CHANGE_NEW_BOX_WEIGHT:
        draft.newBox.weight = action.weight;
        break;
      case CHANGE_NEW_BOX_VALIDATION_RESULT:
        console.log(
          `CHANGE_NEW_BOX_VALIDATION_RESULT: ${JSON.stringify(
            action.validationResult,
          )}`,
        );
        draft.newBoxValidationResult = action.validationResult;
      case SAVE_BOXES:
        if (action.boxes != undefined) {
          draft.boxes2 = action.boxes;
        }
    }
  });

export default homeReducer;
