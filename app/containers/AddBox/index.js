/*
 * AddBox
 *
 * Add new box
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import { RgbColorPicker } from 'react-colorful';

import {
  changeUsername,
  changeNewBoxColorObj,
  changeNewBoxCountry,
  changeNewBoxName,
  changeNewBoxWeight,
  saveNewBox,
} from './actions';

import {
  makeSelectNewBoxValidationResultColor,
  makeSelectNewBoxValidationResultCountry,
  makeSelectNewBoxValidationResultName,
  makeSelectNewBoxValidationResultWeight,
  makeSelectNewBoxCountry,
  makeSelectNewBoxName,
  makeSelectNewBoxWeight,
} from './selectors';

import reducer from './reducer';
import saga from './saga';

import '../../css/app.css';

const key = 'home';

export function AddBox({
  newBoxCountry,
  newBoxName,
  newBoxWeight,
  newBoxNameValidationColor,
  newBoxNameValidationCountry,
  newBoxNameValidationName,
  newBoxNameValidationWeight,

  onChangeNewColorObj,
  onChangeNewBoxCountry,
  onChangeNewBoxName,
  onChangeNewBoxWeight,
  onSaveNewBox,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {}, []);

  const countries = [
    { name: 'Austraila', id: 'Austraila', default: false },
    { name: 'Brazil', id: 'Brazil', default: false },
    { name: 'China', id: 'China', default: false },
    { name: 'Sweden', id: 'Sweden', default: true },
  ];
  const countriesOptions = countries.map(country => (
    <option
      key={`newBoxCountryOption${country.id}`}
      id={`newBoxCountryOption${country.id}`}
      value={country.id}
    >
      {country.name}
    </option>
  ));

  return (
    <article>
      <Helmet>
        <title>Add new box</title>
        <meta name="description" content="Add new box" />
      </Helmet>
      <div>
        <div className="container">
          <div className="row">
            <div className="col col-12 p-2">
              <h2 className="list-title">New Box</h2>
            </div>
          </div>
        </div>
        <div className="container box-form">
          <div className="row">
            <div className="col col-12 p-4">
              <form>
                <div className="mb-3">
                  <label htmlFor="name" className="form-label">
                    Name
                  </label>
                  {newBoxNameValidationName && (
                    <div className="invalid-feedback">
                      {newBoxNameValidationName}
                    </div>
                  )}
                  <input
                    type="text"
                    className="form-control"
                    id="name"
                    value={newBoxName || ''}
                    onChange={onChangeNewBoxName}
                  />
                  <div id="nameHelp" className="form-text">
                    Shipping name
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="country" className="form-label">
                    Country
                  </label>
                  {newBoxNameValidationCountry && (
                    <div className="invalid-feedback">
                      {newBoxNameValidationCountry}
                    </div>
                  )}
                  <select
                    type="text"
                    className="form-select"
                    id="countrySelect"
                    value={newBoxCountry || ''}
                    onChange={onChangeNewBoxCountry}
                  >
                    {countriesOptions}
                  </select>
                  <div id="countryHelp" className="form-text">
                    Shipping country
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="weight" className="form-label">
                    Weight
                  </label>
                  {newBoxNameValidationWeight && (
                    <div className="invalid-feedback">
                      {newBoxNameValidationWeight}
                    </div>
                  )}
                  <input
                    type="text"
                    className="form-control"
                    id="weight"
                    value={newBoxWeight || 0}
                    onChange={onChangeNewBoxWeight}
                  />
                  <div id="weightHelp" className="form-text">
                    Weigt in KG
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="color" className="form-label">
                    Box color
                  </label>
                  {newBoxNameValidationColor && (
                    <div className="invalid-feedback">
                      {newBoxNameValidationColor}
                    </div>
                  )}
                </div>
                <div className="mb-3">
                  <RgbColorPicker onChange={onChangeNewColorObj} />
                </div>
                <div id="colorHelp" className="form-text">
                  Any color except blue
                </div>
                <button
                  type="button"
                  className="btn btn-success float-end"
                  onClick={onSaveNewBox}
                >
                  Save
                </button>
                <Link className="btn float-end me-3" to="./listboxes">
                  Cancel
                </Link>
              </form>
            </div>
          </div>
        </div>
      </div>
    </article>
  );
}

AddBox.propTypes = {
  newBoxCountry: PropTypes.string,
  newBoxName: PropTypes.string,
  newBoxNameValidationColor: PropTypes.string,
  newBoxNameValidationCountry: PropTypes.string,
  newBoxNameValidationName: PropTypes.string,
  newBoxNameValidationWeight: PropTypes.string,
  newBoxWeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChangeNewColorObj: PropTypes.func,
  onChangeNewBoxCountry: PropTypes.func,
  onChangeNewBoxName: PropTypes.func,
  onChangeNewBoxWeight: PropTypes.func,
  onSaveNewBox: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  newBoxCountry: makeSelectNewBoxCountry(),
  newBoxName: makeSelectNewBoxName(),
  newBoxNameValidationColor: makeSelectNewBoxValidationResultColor(),
  newBoxNameValidationCountry: makeSelectNewBoxValidationResultCountry(),
  newBoxNameValidationName: makeSelectNewBoxValidationResultName(),
  newBoxNameValidationWeight: makeSelectNewBoxValidationResultWeight(),
  newBoxWeight: makeSelectNewBoxWeight(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onChangeNewColorObj: colorObj => {
      dispatch(changeNewBoxColorObj(colorObj));
    },
    onChangeNewBoxCountry: evt =>
      dispatch(changeNewBoxCountry(evt.target.value)),
    onChangeNewBoxName: evt => dispatch(changeNewBoxName(evt.target.value)),
    onChangeNewBoxWeight: evt => dispatch(changeNewBoxWeight(evt.target.value)),
    onSaveNewBox: () => dispatch(saveNewBox()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddBox);
