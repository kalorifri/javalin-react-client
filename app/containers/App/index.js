/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { memo } from 'react';
import { Switch, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import AddBox from 'containers/AddBox/Loadable';
import ListBoxes from 'containers/ListBoxes/Loadable';

import List from 'components/List';

import GlobalStyle from '../../global-styles';

import reducer from './reducer';

import { makeSelectBoxes, makeSelectNewBoxColor } from './selectors';

import { changeNewBoxColor } from './actions';

const key = 'App';

export function App({ boxes, newBoxColor, onChangeNewBoxColor }) {
  useInjectReducer({ key, reducer });

  return (
    <div>
      <Switch>
        <Route exact path="/addbox" component={AddBox} />
        <Route exact path="/listboxes" component={ListBoxes} />
        <Route exact path="*" component={ListBoxes} />
      </Switch>
      <GlobalStyle />
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  boxes: makeSelectBoxes(),
  newBoxColor: makeSelectNewBoxColor(),
});

export function mapDistpatchToProps(dispatch) {
  return {
    onNewBox: () => {
      console.log('onNewBox');
    },
    onChangeNewBoxColor: evt => {
      console.log('changeNewBoxColor');
      dispatch(changeNewBoxColor(evt.target.value));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDistpatchToProps,
);

export default compose(
  withConnect,
  memo,
)(App);
