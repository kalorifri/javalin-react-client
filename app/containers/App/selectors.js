import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const selectGlobal = state => state.global || initialState;

const selectRouter = state => state.router;

export const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routerState => routerState.location,
  );

export const makeSelectBoxes = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.boxes
  )

export const makeSelectNewBoxColor = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.newBoxColor
  )

// export { makeSelectLocation };
