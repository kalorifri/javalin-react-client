

import produce from 'immer';
import { CHANGE_NEW_BOX_COLOR } from './constants';

export const initialState = {
    boxes: [
        {
            id: 1,
            name: 'first'
        }
    ],
    newBoxColor: 'red'
};


const appReducer = (state = initialState, action) => {
    produce(state, draft => {

        console.log('action.type: ' + action.type);

        switch (action.type) {
            case CHANGE_NEW_BOX_COLOR:
                console.log('reducer produce test action.color: ' + action.color);
                draft.newBoxColor = action.color;
                draft.boxes.push({
                    id: 2,
                    name: 'second'
                });
                break;
            
        }
    });
    return state;
};

// export default appReducer;


/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case CHANGE_NEW_BOX_COLOR:
                console.log('reducer produce test action.color: ' + action.color + ' draft.newBoxColor: ' + draft.newBoxColor);
                draft.newBoxColor = action.color;
                console.log('reducer produce test  draft.newBoxColor b: ' + draft.newBoxColor);
                // Delete prefixed '@' from the github username
                // draft.username = action.username.replace(/@/gi, '');
                break;
        }
    });

export default homeReducer;

