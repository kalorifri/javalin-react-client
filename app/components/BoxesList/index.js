import React from 'react';

function BoxesList(boxes) {
  let listHeader = '';
  let listContent = (
    <div className="row justify-content-md-center p-4">No boxes found</div>
  );

  if (boxes.boxes && boxes.boxes.length > 0) {
    listHeader = (
      <div className="row justify-content-md-center g-1">
        <div className="col col-lg-1 p-2">Color</div>
        <div className="col col-lg-2 p-2">Receiver</div>
        <div className="col col-lg-2 p-2">Weight (KG)</div>
        <div className="col col-lg-2 p-2">Shipping cost</div>
      </div>
    );

    listContent = boxes.boxes.map(box => (
      <div
        key={`box-${box.id}`}
        id={`box-${box.id}`}
        className="row justify-content-md-center box-row g-1"
      >
        <div className="col col-lg-1 p-2">
          <div
            data-test="lorem"
            className="box-color-circle"
            style={{ backgroundColor: `rgb(${box.color_rgb})` }}
          />
        </div>
        <div className="col col-lg-2 p-2">{box.shipping_address.name}</div>
        <div className="col col-lg-2 p-2">{box.weight}</div>
        <div className="col col-lg-2 p-2">{box.formatted_price}</div>
      </div>
    ));
  }

  return (
    <div className="container boxes-list px-4 py-2">
      {listHeader}
      {listContent}
    </div>
  );
}

export default BoxesList;
