import React from 'react';

/**
 * Simple form validation. Check if empty or invalid values
 * @return object validation result
 */
export default function validate(box)
{
    const nameValidationResult = (box.name !== '') ? '' : 'Please type name';
    const countryValidationResut = (['Australia', 'Brazil', 'China', 'Sweden'].includes(box.country)) ? '' : 'Please select available country';
    const weightValidationResult = (parseFloat(box.weight) == box.weight && box.weight > 0) ? '' : 'Number must be larger than 0';

    let colorValidationResult = (box.color !== '') ? '' : 'Please select color';

    const hasError = (
        nameValidationResult !== '' ||
        colorValidationResult !== '' ||
        countryValidationResut !== '' ||
        weightValidationResult !== ''
    );

    return {
        color: colorValidationResult,
        country: countryValidationResut,
        name: nameValidationResult,
        weight: weightValidationResult,
        hasError: hasError,
    };
};